package main

import (
	"database/sql"
	"log"
	"encoding/json"
	_ "github.com/go-sql-driver/mysql"
	"net/http"
	"github.com/gorilla/mux"
)

func connect() *sql.DB {
	db, err := sql.Open("mysql", "root:@tcp(127.0.0.1:3306)/db_nodejs")
	if err != nil {
		log.Fatal(err)
	}
	return db
}

type Customer_bank struct {
	Cust_id int `json:"cust_id"`
	Nama string `json:"nama"`
	Alamat string `json:"alamat"`
	Kode_pos int `json:"kode_pos"`
	No_hp int `json:"no_hp"`
	Email string `json:"email"`
}

type ResponseAPI struct {
	Status int `json:"status"`
	Message string `json:"message"`
	Data   []Customer_bank `json:"data"`
}

type ResponseAPI2 struct {
	Status int `json:"status"`
	Message string `json:"message"`
}

func returnAllCustomer(w http.ResponseWriter, r *http.Request){
	var cust_bank Customer_bank
	var arr_cust_bank []Customer_bank
	var responseAPI ResponseAPI
	db := connect()
	defer db.Close()

	rows, err := db.Query("Select * from customer_bank ORDER BY cust_id DESC")
	if err!= nil {
		log.Print(err)
	}

	for rows.Next(){
		if err := rows.Scan(&cust_bank.Cust_id, &cust_bank.Nama, &cust_bank.Alamat, &cust_bank.Kode_pos, &cust_bank.No_hp, &cust_bank.Email); err != nil {
			log.Fatal(err.Error())

		}else{
			arr_cust_bank = append(arr_cust_bank, cust_bank)
		}
	}

	responseAPI.Status = 200
	responseAPI.Message = "Success" 
	responseAPI.Data = arr_cust_bank
	
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseAPI)
}

func returnSingleCustomer(w http.ResponseWriter, r *http.Request){
	vars := mux.Vars(r)
	key := vars["id"]
	var cust_bank Customer_bank
	var arr_cust_bank []Customer_bank
	var responseAPI ResponseAPI
	db := connect()
	defer db.Close()

	rows, err := db.Query("Select * from customer_bank where cust_id = ?",key)
	if err!= nil {
		log.Print(err)
	}

	for rows.Next(){
		if err := rows.Scan(&cust_bank.Cust_id, &cust_bank.Nama, &cust_bank.Alamat, &cust_bank.Kode_pos, &cust_bank.No_hp, &cust_bank.Email); err != nil {
			log.Fatal(err.Error())

		}else{
			arr_cust_bank = append(arr_cust_bank, cust_bank)
		}
	}

	responseAPI.Status = 200
	responseAPI.Message = "Success" 
	responseAPI.Data = arr_cust_bank
	
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseAPI)
}

func insertCustomer(w http.ResponseWriter, r *http.Request) {
	var response ResponseAPI2
	
	db := connect()
	defer db.Close()
	
	err := r.ParseMultipartForm(4096)
	if err != nil {
		panic(err)
	}
	
	nama := r.FormValue("nama")
	alamat := r.FormValue("alamat")
	kode_pos := r.FormValue("kode_pos")
	no_hp := r.FormValue("no_hp")
	email := r.FormValue("email")
	
	_, err = db.Exec("INSERT INTO customer_bank (nama, alamat, kode_pos, no_hp, email) values (?,?,?,?,?)",
			nama,
			alamat,
			kode_pos,
			no_hp,
			email,
		)
	
	if err != nil {
		log.Print(err)
	}

	response.Status = 200
	response.Message = "Insert Data Customer Success"
	
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func updateCustomer(w http.ResponseWriter, r *http.Request) {
	var response ResponseAPI2
	db := connect()
	defer db.Close()

	err := r.ParseMultipartForm(4096)
	if err != nil {
		panic(err)
	}

	cust_id := r.FormValue("cust_id")
	nama := r.FormValue("nama")
	alamat := r.FormValue("alamat")
	kode_pos := r.FormValue("kode_pos")
	no_hp := r.FormValue("no_hp")
	email := r.FormValue("email")

	_, err = db.Exec("UPDATE customer_bank set nama = ?, alamat = ?, kode_pos = ?, no_hp = ?, email = ? where cust_id = ?",
		nama,
		alamat,
		kode_pos,
		no_hp,
		email,
		cust_id,
	)

	if err != nil {
		log.Print(err)
	}

	response.Status = 200
	response.Message = "Update Data Customer Success"
	
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func deleteCustomer(w http.ResponseWriter, r *http.Request) {
	var response ResponseAPI2
	
	db := connect()
	defer db.Close()

	err := r.ParseMultipartForm(4096)
	if err != nil {
		panic(err)
	}

	cust_id := r.FormValue("cust_id")

	_, err = db.Exec("DELETE from customer_bank where cust_id = ?", cust_id)

	if err != nil {
		log.Print(err)
	}

	response.Status = 200
	response.Message = "Delete Data Customer Success"

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func main(){
	router := mux.NewRouter()
	router.HandleFunc("/",returnAllCustomer).Methods("GET")
	router.HandleFunc("/{id}",returnSingleCustomer).Methods("GET")
	router.HandleFunc("/",insertCustomer).Methods("POST")
	router.HandleFunc("/",updateCustomer).Methods("PUT")
	router.HandleFunc("/",deleteCustomer).Methods("DELETE")
	log.Fatal(http.ListenAndServe(":1234",router))
}